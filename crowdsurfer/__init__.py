# [ CrowdCube ]
CROWDCUBE_URL = 'https://www.crowdcube.com/investments'

# [ MongoDB ]
MONGODB_URL = 'mongodb://localhost:27017/crowdsurfer'
MONGODB_DB_NAME = 'crowdsurfer'
MONGODB_COLLECTION_NAME = 'campaigns'

# [ KickStarter ]
KICKSTARTER_API_URL = 'https://www.kickstarter.com/discover/categories/{category}'
KICKSTARTER_API_PAGE_SIZE = 20
KICKSTARTER_API_TOTAL_PROJECTS = 100

from crowdsurfer.handler import CrowdSurfer
