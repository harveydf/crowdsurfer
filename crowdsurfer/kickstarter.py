import datetime
from math import ceil
from typing import List

import requests

from crowdsurfer import KICKSTARTER_API_URL, KICKSTARTER_API_TOTAL_PROJECTS, \
    KICKSTARTER_API_PAGE_SIZE


class KickStarter(object):
    def __init__(self, category: str):
        self.url = KICKSTARTER_API_URL.format(category=category.strip().lower())
        self.headers = {'accept': 'application/json'}
        self.params = {
            'seed': 2461239,
            'sort ': 'magic',
            'woe_id': 0,
        }
        self.pages = ceil(
            KICKSTARTER_API_TOTAL_PROJECTS / KICKSTARTER_API_PAGE_SIZE)

    def _get_data(self) -> List[dict]:
        """
        Call the kickstarter's api and return the data

        :return: the projects' data
        """
        data = []

        for page in range(1, self.pages + 1):
            params = {**self.params, 'page': page}
            result = requests.get(self.url, params=params, headers=self.headers)

            if result.status_code != 200:
                raise Exception("The Kick Starter's API couldn't be reached.")

            data += result.json()['projects']

        return data

    def _parse_data(self, projects: List[dict]) -> List[dict]:
        """
        Extract the needed data

        :param projects: the kick starter's projects in json format
        :return: the data needed to store in mongo

        """
        data = []

        for project in projects:
            # Calculate the percentage
            pledge = float(project['pledged'])
            percentage = (pledge * 100) / float(project['goal'])

            # Calculate the remain days
            delta = datetime.date.fromtimestamp(project['deadline']) - datetime.date.today()

            data.append({
                'title': project['name'],
                'summary': project['blurb'],
                'amount': pledge,
                'percentage': percentage,
                'days_left': delta.days,
                'link': project['urls']['web']['project'],
                'source': 'kickstarter',
            })

        return data

    def get_parsed_data(self):
        api_data = self._get_data()
        return self._parse_data(projects=api_data)
