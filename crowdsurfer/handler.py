from typing import List

from crowdsurfer.crowdcube import CrowdCube
from crowdsurfer.db import DataBase
from crowdsurfer.kickstarter import KickStarter


class CrowdSurfer(object):

    def __init__(self):
        self.db = DataBase()
        self.DAYS_LEFT = 10
        self.CURRENCY = {
            'crowdcube': 'GBP',
            'kickstarter': 'USD',
        }

    def _get_crowdcube_data(self) -> List[dict]:
        crowdcube = CrowdCube()
        return crowdcube.get_scraped_data()

    def _get_kickstarter_data(self, category :str) -> List[dict]:
        kickstarter = KickStarter(category=category)
        return kickstarter.get_parsed_data()

    def _store_data(self, crowdcube_data: List[dict], kickstarter_data: List[dict]):
        self._clear_db()

        data = [*crowdcube_data, *kickstarter_data]
        self.db.save(data)

    def _get_total(self) -> List[dict]:
        result = self.db.collection.aggregate([
            {"$match": {"days_left": {"$gte": self.DAYS_LEFT}}},
            {"$group": {"_id": "$source", "total": {"$sum": "$amount"}}}
        ])

        return list(result)

    def _clear_db(self):
        self.db.collection.remove()

    def main(self, category :str='art'):
        """

        - Get the data from CrowdCube and KickStarter
        - Store the data in MongoDB
        - Return the sum of amounts for the projects that have at least
          10 days remaining

        :param category: The name of the category for KickStarter

        """

        # Get CrowdCube data
        crowdcube_data = self._get_crowdcube_data()

        # Get KickStarter data
        kickstarter_data = self._get_kickstarter_data(category=category)

        # Concat and store the data in mongo
        self._store_data(crowdcube_data, kickstarter_data)

        # Print the resulting data
        for total in self._get_total():
            source = total['_id']

            print('{source}: {currency} ${amount:,.2f}'.format(
                source=source.upper(),
                currency=self.CURRENCY[source],
                amount=total['total'],
            ))
