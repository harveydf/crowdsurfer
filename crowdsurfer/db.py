from typing import List

import pymongo

from crowdsurfer import MONGODB_URL, MONGODB_DB_NAME, MONGODB_COLLECTION_NAME


class DataBase(object):

    def __init__(self):
        self.client = pymongo.MongoClient(MONGODB_URL)
        self.db = self.client.get_database(MONGODB_DB_NAME)
        self.collection = self.db.get_collection(MONGODB_COLLECTION_NAME)

    def save(self, data: List[dict]):
        """
        Store the data in Mongo DB

        :param data: the scraped data
        """

        self.collection.insert_many(data)

    def get_total(self) -> List[dict]:
        """
        Calculate the total amount for docs that have at least 10 days left
        for both platforms

        :return: total amount
        """
        result = self.collection.aggregate([
            { "$match": { "days_left": { "$gte": 10 } } },
            { "$group": { "_id": "$source", "total": { "$sum": "$amount" } } }
        ])

        return list(result)
