from typing import List

import requests
from bs4 import BeautifulSoup

from crowdsurfer import CROWDCUBE_URL


class CrowdCube(object):
    def __init__(self):
        self.url = CROWDCUBE_URL

    def _get_data(self) -> bytes:
        """
        Get the HTML content of the URL
        :rtype: bytes
        """
        response = requests.get(self.url)

        if response.status_code != 200:
            raise Exception('The URL can\'t be reached.')

        return response.content

    def _scrape_data(self, content: bytes) -> List[dict]:
        """
        Scrape the data from the content and organize it for storage

        :param content: The HTML content in bytes
        :return: a list of dictionaries with the scraped data

        """

        soup = BeautifulSoup(content, 'html.parser')
        sections = soup.find_all('section')

        data = []

        for section in sections:
            data.append({
                'title': section.attrs['data-opportunity-name'],
                'summary': section.select_one('.cc-card__content > p').get_text(),
                'amount': float(section.attrs['data-opportunity-raised']),
                'percentage': float(section.attrs['data-opportunity-progress']),
                'days_left': int(section.select_one('.cc-card__daysleft')
                                 .get_text().split(' ')[0]),
                'link': section.a['href'],
                'source': 'crowdcube',
            })

        return data

    def get_scraped_data(self) -> List[dict]:
        """
        Request and scrape the data

        :return: The scraped data
        """

        content = self._get_data()
        return self._scrape_data(content=content)
