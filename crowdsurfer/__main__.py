import argparse

from crowdsurfer.handler import CrowdSurfer

parser = argparse.ArgumentParser()
parser.add_argument("-c", "--category", help="the kickstarter's category name")

args = parser.parse_args()
category = args.category

surfer = CrowdSurfer()
surfer.main(category=category)
