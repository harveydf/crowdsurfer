from setuptools import setup
from codecs import open
from os import path

base_dir = path.abspath(path.dirname(__file__))

with open(path.join(base_dir, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='crowdsurfer',
    version='1.0.0',
    description='Get the data from CrowCube and KickStarter to analise it',
    long_description=long_description,
    url='https://gitlab.com/harveydf/crowdsurfer',
    author='Harvey David Forero',
    author_email='harvey.forero@gmail.com',
    license='MIT',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
    ],
    install_requires=['beautifulsoup4', 'pymongo', 'requests'],
    entry_points={
        'console_scripts': [
            'crowdsurfer=crowdsurfer',
        ],
    },
)
